<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Karyawan::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'harian' => rand(0,1) * 20000,
        'bulanan' => rand(0,1) * 500000,
        'kerajinan' => 300000,
    ];
});
