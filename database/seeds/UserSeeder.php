<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	\App\User::truncate();
    	
        \App\User::create(['name'=>'Admin', 'email'=>'admin@cismis.test', 'password'=>bcrypt('adminadmin'), 'group_id'=>1 ]);

        factory(App\User::class, 50)->create();
    }
}
