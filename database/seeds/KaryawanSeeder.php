<?php

use Illuminate\Database\Seeder;

class KaryawanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	\App\Karyawan::truncate();

        //\App\Karyawan::create(['name'=>'Admin', 'email'=>'admin@cismis.test', 'password'=>bcrypt('adminadmin'), 'group_id'=>1 ]);

        factory(App\Karyawan::class, 5)->create();
    }
}
