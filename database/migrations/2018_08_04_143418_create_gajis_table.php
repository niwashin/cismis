<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGajisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gajis', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('bulan');
            $table->integer('tahun');
            $table->integer('hari_kerja');

            $table->float('kerajinan', 15,2)->default(0);
            $table->float('sisacashbon', 15,2)->default(0);
            $table->float('sisagaji', 15,2)->default(0);

            $table->integer('karyawan_id')->nullable();

            $table->index(['karyawan_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gajis');
    }
}
