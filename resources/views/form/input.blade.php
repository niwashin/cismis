@switch( \DB::connection()->getDoctrineColumn( $new_model->getTable() , $each )->getType()->getName() )
    @case('string')
        <input class="form-control" type="text" name="{{ $each }}" @include('form.default_value') />
        @break

    @case('integer')
        <input class="form-control text-right" type="number" name="{{ $each }}" @include('form.default_value') />
        @break

    @case('float')
        <input class="form-control text-right decimal" type="text" autocomplete="off" name="{{ $each }}" @include('form.default_value', ['default_decimal' => 0]) />
        @break

    @case('decimal')
        <input class="form-control text-right decimal" type="text" autocomplete="off" name="{{ $each }}" @include('form.default_value', ['default_decimal' => 0]) />
        @break

    @case('date')
        <input class="form-control text-right" type="date" name="{{ $each }}" @include('form.default_value', ['default_decimal' => 0]) />
        @break

    @default
@endswitch