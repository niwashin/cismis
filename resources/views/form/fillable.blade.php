<table class="table table-hover table-bordered">
    @foreach($new_model->getFillable() as $key => $each)
        @if( substr($each, -3) <> '_id' )
            <tr>
                <td>
                    {{ $each }}
                </td>
                <td>
                    @if(\Schema::hasColumn( $new_model->getTable() , $each ))
                        
                        @include('form.input')
                    @else
                        <span class="text-danger">Column not exist</span>
                    @endif
                </td>
            </tr>
        @endif
    @endforeach
</table>