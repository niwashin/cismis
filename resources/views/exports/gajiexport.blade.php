<table>
    <thead>
    <tr>
        <th>No</th>
        <th>Name</th>
        <th>Hari Kerja</th>
        <th>Gaji</th>
        <th>Cashbon</th>
        <th>Kerajinan</th>
        <th>Total</th>
        <th>Bulan</th>
        <th>Tahun</th>
        <th>Hari</th>
    </tr>
    </thead>
    <tbody>
        @foreach($gajis as $key => $gaji)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $gaji->karyawan->name }}</td>
                <td>{{ $gaji->hari_kerja }}</td>
                <td>{{ $gaji->hari_kerja * $gaji->karyawan->harian }}</td>
                <td>{{ $gaji->cashbon }}</td>
                <td>{{ $gaji->hari_kerja == \App\Bulan::where('bulan', $gaji->bulan )->where('tahun', $gaji->tahun )->first()->hari_kerja ? $gaji->karyawan->kerajinan : 0 }}</td>
                <td>{{ $gaji->nilai }}</td>
                <td>{{ $gaji->bulan }}</td>
                <td>{{ $gaji->tahun }}</td>
                <td>{{ \App\Bulan::where('bulan', $gaji->bulan )->where('tahun', $gaji->tahun )->first()->hari_kerja }}</td>
            </tr>
        @endforeach
    </tbody>
</table>