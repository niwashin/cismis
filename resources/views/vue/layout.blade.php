<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Required Stylesheets -->
    <link type="text/css" rel="stylesheet" href="https://unpkg.com/bootstrap/dist/css/bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="https://unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.css"/>

    <!-- Required scripts -->
    <script src="https://unpkg.com/vue"></script>
    <script src="https://unpkg.com/babel-polyfill@latest/dist/polyfill.min.js"></script>
    <script src="https://unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.js"></script>
  </head>

  <body>
    <!-- Our application root element -->
    <div id="app">
      <div class="container">
        <div>
          <!-- Image and text -->
          <b-navbar variant="faded" type="light">
            <b-navbar-brand href="#">
              <img src="https://placekitten.com/g/30/30" class="d-inline-block align-top" alt="BV">
              BootstrapVue
            </b-navbar-brand>
          </b-navbar>
        </div>

        <div>
          <b-card-group columns>
              <b-card title="Card title that wraps to a new line"
                      img-src="https://placekitten.com/g/400/450"
                      img-fluid
                      img-alt="image"
                      img-top>
                  <p class="card-text">
                      This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit
                      longer.
                  </p>
              </b-card>
              <b-card header="Quote">
                  <blockquote class="blockquote mb-0">
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                      <footer class="blockquote-footer">Someone famous in <cite title="Source Title">Source Title</cite></footer>
                  </blockquote>
              </b-card>
              <b-card title="Title"
                      img-src="https://placekitten.com/500/350"
                      img-fluid
                      img-alt="image"
                      img-top>
                  <p class="card-text">
                      This card has supporting text below as a natural lead-in to additional content.
                  </p>
                  <small class="text-muted">Last updated 3 mins ago</small>
              </b-card>
              <b-card bg-variant="primary"
                      text-variant="white">
                  <blockquote class="card-blockquote">
                      <p>
                          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.
                      </p>
                      <footer>
                          <small>Someone famous in <cite title="Source Title">Source Title</cite></small>
                      </footer>
                  </blockquote>
              </b-card>
              <b-card title="Title">
                  <p class="card-text">
                      This card has supporting text below as a natural lead-in to additional content.
                  </p>
                  <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
              </b-card>
              <b-card img-src="https://picsum.photos/400/400/?image=41"
                      img-fluid
                      img-alt="image"
                      overlay>
              </b-card>
              <b-card img-src="https://picsum.photos/400/200/?image=41"
                      img-fluid
                      img-alt="image"
                      img-top>
                  <p class="card-text">
                      This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content
                      than the first.
                  </p>
                  <div slot="footer">
                      <small class="text-muted">Footer Text</small>
                  </div>
              </b-card>
          </b-card-group>
      </div>
      </div>

      @yield('content')
    </div>

    <!-- Start running your app -->
    <script>
      window.app = new Vue({
        el: "#app",
        data: {
          name: 'app'
        }
      })
    </script>

  </body>
</html>