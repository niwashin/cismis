@extends('layouts.app')

@section('js')
    <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/autonumeric@4.1.0"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/mark.js/8.11.1/jquery.mark.min.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

    <script type="text/javascript">
        $( document ).ready(function() {
            AutoNumeric.multiple('input.decimal', { 
                digitGroupSeparator        : ',',
                decimalCharacter           : '.', });

            $('a.confirm').click(function(event){
                return confirm( "Confirm Delete" );
            });


            $("#select-hasmany").change(function()
            {
                var value = $(this).val();

                axios.get('/api/model', {
                    params: {
                        name: "{{ ucfirst($model_name) }}",
                        rel: value
                    }
                  })
                  .then(function (response) {
                    $('#table-hasmany').html(response.data);
                  })
                  .catch(function (error) {
                    console.log(error);
                  })
                  .then(function () {
                    alert(value);
                  }); 
            });

            $("div.card-header").click(function()
            {
                $(this).next().toggle();
            });
        });

        @if($request->q <> '')
            $(".table-result td").mark("{{ $request->q }}");
        @endif
    </script>

    <style type="text/css">
        mark{
            background: orange;
            color: black;
        }
    </style>
@endsection

@section('content')

    <div class="container">
        <div class="row justify-content-center">

            <div class="col-md-12 mb-3">
                <a class="btn btn-secondary" href="{{ route('home') }}" role="button">Home</a>
                <a class="btn btn-secondary" href="{{ route('index',[$model_name]) }}" role="button">{{ ucfirst($model_name) }}</a>
            </div>

            @if (session('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @if($new_model->getFillable())
                <div class="col-md-12 mb-3">
                    <div class="card">
                        <div class="card-header">
                            <span class="oi oi-plus"></span>
                        </div>


                        <div class="card-body">
                            <form method="post" action="{{ route('create', $model_name) }}">
                                @csrf

                                @if( isset($new_model::$belongsTo) )
                                    <span>Belongs To</span>

                                    <table class="table table-hover table-bordered">
                                        @foreach($new_model::$belongsTo as $each)
                                            @php
                                                $belongsto_model = 'App\\'.ucfirst($each);
                                            @endphp
                                            
                                            <tr>
                                                <td>{{ $each }}</td>
                                                <td>
                                                    <select class="form-control" name="{{ $each }}_id">
                                                        @foreach( $belongsto_model::get() as $each_belongsto )
                                                            <option value="{{ $each_belongsto->id }}">[{{ $each_belongsto->id }}] {{ $each_belongsto->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                @endif

                                @if( isset($new_model::$belongsToMany) )
                                    <span>Belongs To Many</span>

                                    <table class="table table-hover table-bordered">
                                        <tr>
                                            <td>Model</td>
                                            <td>
                                                <select class="form-control">
                                                    <option>[id] Name Description</option>
                                                    <option>[id] Name Description</option>
                                                </select>
                                            </td>
                                        </tr>
                                    </table>
                                @endif

                                @include('form.fillable')

                                @if( isset($new_model::$hasMany) )
                                    <span>Has Many</span>

                                    <table class="table table-hover table-bordered">
                                        <tr>
                                            <td>
                                                <select class="form-control" id="select-hasmany" name="hasMany">
                                                    <option>null</option>

                                                    @foreach($new_model::$hasMany as $each)
                                                        <option>{{ $each }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                        </tr>
                                    </table>

                                    <div id="table-hasmany"></div>
                                @endif

                                <table class="table table-bordered">
                                    <tr>
                                        <td colspan="2" class="text-center">
                                            <input type="submit" value="Submit" class="btn btn-info" {{ $fillable ? '' : 'disabled' }}>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>

                @if(in_array('Nicolaslopezj\Searchable\SearchableTrait', class_uses($new_model)))
                    <div class="col-md-12 mb-3 form-group">
                        <form action="{{ url()->current() }}" method="get">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search keyword" aria-label="keyword" aria-describedby="basic-addon2" name="q" value="{{ $request->q }}">
                                <div class="input-group-append">
                                    <input class="btn btn-outline-secondary" type="submit" value="Search">
                                </div>
                            </div>
                        </form>
                    </div>
                @endif
            @endif

            <div class="col-md-12 mb-3 form-group">
                <form action="{{ url()->current() }}" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="SQL Where. Example : id > 100" aria-label="SQL Where" aria-describedby="basic-addon2" name="sql" value="{{ $request->sql }}">
                        <div class="input-group-append">
                            <input class="btn btn-outline-secondary" type="submit" value="Filter">
                        </div>
                    </div>
                </form>
            </div>

            <div class="col-md-12 mb-2">
                <div class="card">
                    <div class="card-header">
                        <strong>{{ ucfirst($model_name) }}</strong>

                        <a class="float-right" href="{{ url('index/'.$model_name.'?sort='. $request->sort. '&download=xls') }}">Download XLS</a> 
                        <a class="float-right">&nbsp;&bull;&nbsp;</a>
                        <span class="float-right">{{ $collection->total() }} results</span> 
                    </div>

                    @if($collection->count())
                        <div class="card-body pl-0 pr-0" style="overflow-x: auto;">
                            <table class="table table-striped table-result">
                                <tr>
                                    <th>
                                        #
                                    </th>

                                    @foreach($collection->first()->toArray() as $att => $value)
                                        @if(\Schema::hasColumn( $new_model->getTable() , $att ))
                                            @if( substr($att, -3) == '_id' )
                                                <th>
                                                    <a href="{{ url('index/'. substr($att, 0, -3) ) }}">
                                                        {{ ucfirst( substr($att, 0, -3) ) }}
                                                    </a>
                                                </th>
                                            @else
                                                <th>
                                                    {{ ucfirst( $att) }} 
                                                    <a href="{{ route('index', $model_name) }}?{{ $request->has('sql') ? 'sql='.$request->sql.'&' : '' }}sort={{ $att }}"><span class="oi oi-arrow-circle-top"></span></a>
                                                    <a href="{{ route('index', $model_name) }}?{{ $request->has('sql') ? 'sql='.$request->sql.'&' : '' }}sort={{ $att }}&order=desc"><span class="oi oi-arrow-circle-bottom"></span></a>
                                                </th>
                                            @endif
                                        @else
                                            <th>
                                                {{ ucfirst($att) }}
                                            </th>
                                        @endif
                                    @endforeach

                                    <th class="text-center">
                                        <span class="oi oi-menu"></span>
                                    </th>

                                </tr>

                                @foreach($collection as $key => $each)
                                    <tr>
                                        <td>{{ $key+1 + ((($request->page ? $request->page : 1 ) - 1) * $row_per_page)}}</td>

                                        @foreach($each->toArray() as $att => $value)
                                            @if(\Schema::hasColumn( $new_model->getTable() , $att ))
                                                @if( substr($att, -3) == '_id' )
                                                    @php
                                                        $rel_model_name = substr($att, 0,-3);
                                                        $rel_model = '\App\\'.ucfirst( $rel_model_name );
                                                    @endphp
                                                    
                                                    <td>
                                                        <a href="{{ url('index/'.$rel_model_name.'?sql=id='.$value) }}">{{ $rel_model::find($value)->name or 'NULL' }}</a>
                                                    </td>
                                                @else
                                                    @if( 
                                                        (\DB::connection()->getDoctrineColumn( $new_model->getTable() , $att )->getType()->getName() == 'float') or
                                                        (\DB::connection()->getDoctrineColumn( $new_model->getTable() , $att )->getType()->getName() == 'decimal')
                                                    )
                                                        <td class="text-right">{{ number_format( floatval($value), 2) }}</td>
                                                    @else
                                                        <td>{{ $value }}</td>
                                                    @endif
                                                @endif
                                            @else
                                                @if(is_numeric($value))
                                                    <td class="text-right">{{ number_format($value) }}</td>
                                                @else
                                                    <td>{{ $value }}</td>
                                                @endif
                                            @endif
                                        @endforeach

                                        <td class="text-center">
                                            <a class="btn btn-default text-danger confirm" href="{{ route('delete', ['model'=> $model_name, 'id'=> $each->id ]) }}">
                                                <span class="oi oi-circle-x"></span>                                        
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    @else
                        <div class="card-body">
                            <p class="text-center">Empty</p>
                        </div>
                    @endif
                </div>
            </div>

            {{ $collection->links() }}
        </div>
    </div>

@endsection
