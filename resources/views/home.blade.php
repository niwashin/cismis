@extends('layouts.app')

@section('title')
    <h2>Home</h2>
@endsection

@section('js')
    <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script type="text/javascript">
        $( document ).ready(function() {
            $( ".artisan" ).click(function(event) 
            {
                event.preventDefault();
                console.log('artisan clicked');

                var classname = $(this).attr('data-classname');
                var html = $(this).html();
                var component = $(this);

                $(this).html("loading...");

                axios.get('/api/artisan/' + classname)
                    .then(function (response) {
                        console.log(response);
                        alert(response.data);
                    })
                    .catch(function (error) {
                        console.log(error);
                    })
                    .then(function () {
                        console.log('axios worked');

                        component.html(html);
                });  
            });
        });

    </script>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><strong>Model</strong></div>

                    <div class="card-body">

                        <div class="list-group">
                            @foreach($models as $key => $model)
                                @php
                                    $eloquent = '\App\\'. $model;

                                    $eloquent = new $eloquent;

                                @endphp

                                @if(! \Schema::hasTable( $eloquent->getTable() ) )
                                    <li class="list-group-item list-group-item-danger" href="#">
                                        {{ $model }}

                                        <span class="text-danger float-right"><em>Table not found, please run migrate</em></span>
                                    </li>
                                @else
                                    @unless($eloquent->show === false)
                                        <a class="list-group-item list-group-item-action" href="{{ route('index', lcfirst($model)) }}">
                                            {{ $model }}

                                            @if($eloquent::count())
                                                <span class="badge badge-secondary float-right">{{ $eloquent::count() }}</span>
                                            @endif
                                        </a>
                                    @endunless
                                @endif

                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="card" style="margin-top: 25px;">
                    <div class="card-header"><strong>Excel Download</strong></div>

                    <div class="card-body">

                        <div class="list-group">
                            @foreach($xls as $key => $export)
                                @php
                                    $eloquent = '\App\\Exports\\'. $export;

                                    $eloquent = new $eloquent;

                                @endphp

                                <a class="list-group-item list-group-item-action" href="{{ route('download', $export) }}">
                                    {{ $export }}
                                </a>

                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="card" style="margin-top: 25px;">
                    <div class="card-header"><strong>Artisan Command</strong></div>

                    <div class="card-body">

                        <div class="list-group">
                            @foreach($commands as $key => $command)

                                @php
                                    $artisan = '\App\\Console\\Commands\\'. $command;

                                    $artisan = new $artisan;

                                    $check = new ReflectionProperty($artisan, 'description');
                                @endphp
                                
                                <a class="list-group-item list-group-item-action artisan" href="" data-classname="{{ $command }}">
                                    {{ $command }}

                                    @if($check->isPublic() )
                                        <span class="float-right"><em>{{ $artisan->description }}</em></span>
                                    @endif
                                </a>

                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
