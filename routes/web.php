<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('vue.home');
});

Auth::routes();

Route::get('/home', 'HomeController@home')->name('home');
Route::get('/index/{model}', 'HomeController@index')->name('index');
Route::post('/create/{model}', 'HomeController@create')->name('create');
Route::get('/delete/{model}/{id}', 'HomeController@delete')->name('delete');

Route::get('/download/{class}', 'HomeController@download')->name('download');

Route::get('/suggest/{words}', 'HomeController@spellSuggest');