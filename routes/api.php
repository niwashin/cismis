<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/model', function (Request $request) 
{
	$model_name = '\App\\'. ucfirst($request->name);
        
    $model = new $model_name;

    $rel_name = $request->rel;

    if (! in_array($rel_name, $model::$hasMany)) 
    {
    	return '';
    }

    $related_model = get_class($model->$rel_name()->getRelated());

    $new_model = new $related_model;

    return view('form.fillable', compact('new_model'));
});

Route::get('/artisan/{class}', function (Request $request, $class) 
{
	$artisan_name = '\App\Console\Commands\\'.$class;
	$artisan = new $artisan_name;

    $check = new ReflectionProperty($artisan, 'signature');

    if ($check->isPublic()) 
    {
        $signature = $artisan->signature;

        $explode = explode(' ', $signature);

        \Artisan::call($explode[0]);

    }else{
        return 'Failed. Signature is not public.';
    }


    return 'Executed!';
});