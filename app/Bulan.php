<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bulan extends Model
{
    protected $fillable = ['bulan','tahun','hari_kerja'];

    protected $hidden = ['id','created_at','updated_at'];
}
