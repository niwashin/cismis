<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Absen extends Model
{
	protected $today;

	protected $fillable = ['karyawan_id','tanggal','keterangan'];

    protected $hidden = ['id','created_at','updated_at'];

    protected $dates = ['tanggal'];

    static $belongsTo = ['karyawan'];

	public $default_value;

	public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

		$this->default_value = [ 'tanggal' => now()->format('Y-m-d') ];
	}

    public function karyawan()
    {
        return $this->belongsTo('App\Karyawan');
    }
}
