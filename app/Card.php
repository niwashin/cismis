<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Card extends Model
{
	use SearchableTrait;

    public $show = false;

    protected $fillable = [
        'father','mother','brother','sister','address'
    ];

    protected $hidden = [
        'id','created_at','updated_at',
    ];

    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'father' => 10,
            'mother' => 10,
            'brother' => 50,
            'sister' => 5,
            'address' => 5,
        ],
        // 'joins' => [
        //     'posts' => ['users.id','posts.user_id'],
        // ],
    ];
}
