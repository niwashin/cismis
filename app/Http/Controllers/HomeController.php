<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ModelExport;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function spellSuggest($string)
    {
        // Suggests possible words in case of misspelling
        $config_dic = pspell_config_create('id');

        // Ignore words under 3 characters
        pspell_config_ignore($config_dic, 3);

        // Configure the dictionary
        pspell_config_mode($config_dic, PSPELL_FAST);
        $dictionary = pspell_new_config($config_dic);

        // To find out if a replacement has been suggested
        $replacement_suggest = false;

        $string = explode(' ', trim(str_replace(',', ' ', $string)));// repalce any commas with spaces and explode into an array
        foreach ($string as $key => $value) {// loop through each word
            if(!pspell_check($dictionary, $value)) {// check againt existance in dictionary, if not found
                $suggestion = pspell_suggest($dictionary, $value); //add word for suggesstion

                // Suggestions are case sensitive. Grab the first one.
                if(isset($suggestion [0]) && (strtolower($suggestion [0]) != strtolower($value))) {
                    $string [$key] = $suggestion [0];
                    $replacement_suggest = true;
                }
            }
        }

        if ($replacement_suggest) {
            // We have a suggestion, so we return to the data.
            return implode(' ', $string);// combine the string back
        } else {
            return null;
        }
    }

    public function home()
    {
        $files = scandir( app_path() );
        $xls_files = scandir( app_path().'/Exports' );
        $artisan_files = scandir( app_path().'/Console/Commands' );

        $models = array();
        $namespace = '';
        
        foreach($files as $file) 
        {
            //skip current and parent folder entries and non-php files
            if ($file == '.' || $file == '..' || !preg_match('/\.php/', $file)) continue;

            $models[] = $namespace . preg_replace('/\.php$/', '', $file);
        }

        $models = collect($models);

        //--
        
        $xls = array();
        $namespace = '';
        
        foreach($xls_files as $file) 
        {
            //skip current and parent folder entries and non-php files
            if ($file == '.' || $file == '..' || !preg_match('/\.php/', $file)) continue;
            if ($file == 'ModelExport.php') continue;

            $xls[] = $namespace . preg_replace('/\.php$/', '', $file);
        }

        $xls = collect($xls);

        //--
        
        $commands = array();

        foreach($artisan_files as $file) 
        {
            //skip current and parent folder entries and non-php files
            if ($file == '.' || $file == '..' || !preg_match('/\.php/', $file)) continue;

            $commands[] = $namespace . preg_replace('/\.php$/', '', $file);
        }

        $commands = collect($commands);

        return view('home', compact('models','xls','commands'));
    }

    public function index(Request $request, $model_name)
    {
        $model      = '\App\\'. ucfirst($model_name);
        
        $new_model = new $model;

        if ($new_model->row_per_page) 
        {
            $row_per_page = $new_model->row_per_page;
        }else{
            $row_per_page = env('ROW_PER_PAGE', 15);
        }

        $collection = $model::select();

        if ($request->has('q')) 
        {
            # prioritize matches that include the multi-word search (thus, without splitting into words)
            $collection = $collection->search( '%'.$request->q.'%' ,null,true);
        }

        if ($request->has('sql')) 
        {
            $collection = $collection->whereRaw( $request->sql );
        }

        if (($request->has('sort')) and ($request->sort <> ''))
        {
            if ($request->has('order')) 
            {
                $order = 'desc';
            }else{
                $order = 'asc';
            }

            $collection = $collection->orderBy( $request->sort, $order );
        }else{

            if ($request->has('q')) 
            {
                //$collection = $collection->latest();
            }else{
                $collection = $collection->latest();
            }
        }

        if ($request->has('download')) 
        {
            return ($collection->get())->downloadExcel(
                $filePath = 'model.xlsx',
                $writerType = 'Xlsx',
                $headings = true
                );
        }


        $collection = $collection->paginate( $row_per_page );
        //return $collection->first();
        
        $fillable = true;

        foreach($new_model->getFillable() as $key => $each)
        {
            if(! \Schema::hasColumn( $new_model->getTable() , $each ))
            {
                $fillable = false;
            }
        }
        
        return view('index', compact('model_name','collection','new_model','model','request','fillable','row_per_page'));
    }

    public function create(Request $request, $model_name)
    {
        $model      = '\App\\'. ucfirst($model_name);
        $new_model  = new $model; 

        # 
        # need this block to clear comma from html input
        # 
        foreach ($request->all() as $key => $input) 
        {
            if (in_array($key, $new_model->getFillable() )) 
            {
                switch (\DB::connection()->getDoctrineColumn( $new_model->getTable() , $key )->getType()->getName()) 
                {
                    case('decimal'):
                    case('float'):
                        $request->merge([$key => str_replace(',', '', $request->get( $key ) )]);
                        break;
                    
                    default:
                        # no plan yet
                        break;
                }
            }
        }

        if(property_exists($model, 'validation_rule'))
        {
            $validatedData = $request->validate( $model::$validation_rule );
        }

        $created_model = $model::create( $request->all() );

        if ($request->has('hasMany')) 
        {
            if ($request->hasMany <> 'null') 
            {
                $rel = $request->hasMany;

                $created_model->$rel()->create( $request->all() );
            }            
        }

        return redirect(route('index', $model_name), $status = 302, $headers = [], $secure = null);
    }

    public function delete(Request $request, $model_name, $id)
    {
        $model = '\App\\'. ucfirst($model_name);
        
        $status = $model::findOrFail($id)->delete();

        if ($status) 
        {
            return redirect(route('index', $model_name))->with('message','Deleted!');
        }else{
            return 'dunno, failed';
        }

    }

    public function download($class) 
    {
        $class_name = '\App\Exports\\'.$class;

        return Excel::download(new $class_name, $class.'.xlsx');
    }
}
