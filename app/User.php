<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Nicolaslopezj\Searchable\SearchableTrait as SearchLocal;
use Remoblaser\Search\SearchableTrait as SearchGlobal;

class User extends Authenticatable
{
    use Notifiable;
    use SearchLocal;
    use SearchGlobal;

    public $show = true;

    /**
     * The attributes that are mass assignable.
     * Each value will be shown as input field.
     *
     * @var array
     */
    protected $fillable = [
        //'name', 'email', 'password', 'age', 'money', 'health', 'group_id'
        'name', 'email', 'password', 'group_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     * Each value will not be shown in index table.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'created_at', 'updated_at', 'id', 'group_id'
    ];

    public $default_value;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->default_value = [ 'tanggal' => now()->format('Y-m-d') ];
    }

    /**
     * Laravel form validation will use these rules.
     *
     * @var array
     */
    static $validation_rule = [
        'name' => 'required|unique:items,name|max:255',
        'age' => 'required|numeric',
    ];

    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'name' => 10,
            'email' => 5,
            // 'brother' => 50,
            // 'sister' => 5,
            // 'address' => 5,
        ],
        // 'joins' => [
        //     'posts' => ['users.id','posts.user_id'],
        // ],
    ];

    # SearchGlobal fields
    protected $searchFields = [
        'name',
        'email',
        // 'age',
        // 'money',
        // 'healty'
    ];    


    /**
     * List all of your defined relationship here.
     *
     * @var array
     */
    static $belongsTo        = ['group','race'];
    static $hasMany          = ['addresses'];
    static $belongsToMany    = ['roles'];

    public function group()
    {
        return $this->belongsTo('App\Group');
    }
    public function race()
    {
        return $this->belongsTo('App\Race');
    }

    public function addresses()
    {
        return $this->belongsTo('App\Address');
    }

    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }

    public function scopeWithAll($query) 
    {
        $query->with($this->belongsTo)->with($this->hasMany)->with($this->belongsToMany);
    }
}
