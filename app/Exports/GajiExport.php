<?php

namespace App\Exports;

use App\Gaji;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class GajiExport implements FromView
{
    public function view(): View
    {
        return view('exports.gajiexport', [
            'gajis' => Gaji::where('bulan', \Carbon\Carbon::now()->month)->where('tahun', \Carbon\Carbon::now()->year)->orderBy('karyawan_id')->get()
        ]);
    }
}
