<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;

class ModelExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return \App\Gaji::all();
    }
}
