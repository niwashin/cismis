<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Race extends Model
{
	public $show = false;
	
    protected $fillable = [
        'name', 'description'
    ];
}
