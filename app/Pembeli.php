<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembeli extends Model
{
    public $show = false;

    protected $fillable = ['name','address','phone'];

    protected $casts = [
    	'phone' => 'string'
    ];
}
