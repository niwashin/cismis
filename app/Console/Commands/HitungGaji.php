<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class HitungGaji extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $signature = 'hitung:gaji {--bulan : Bulan akan dihitung} {--tahun : Tahun akan dihitung}';

    /**
     * The console command description.
     *
     * @var string
     */
    public $description = 'Hitung gaji berdasarkan absen';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $bulan = $this->option('bulan');
        $tahun = $this->option('tahun');

        if (! $bulan) 
        {
            $bulan = \Carbon\Carbon::now()->month;
        }

        if (! $tahun) 
        {
            $tahun = \Carbon\Carbon::now()->year;
        }

        $this->line('Menghitung gaji bulan '.$bulan.'/'.$tahun);

        #--

        $karyawans = \App\Karyawan::all();

        $bar = $this->output->createProgressBar(count($karyawans));

        foreach ($karyawans as $karyawan) 
        {
            $month = \App\Bulan::where('tahun', $tahun)->where('bulan', $bulan)->first();

            $hari_kerja = $month->hari_kerja - $karyawan->absens()->whereMonth('tanggal', str_pad($bulan, 2, "0", STR_PAD_LEFT))->count();

            $pokok = $hari_kerja * $karyawan->harian + $karyawan->bulanan;

            if ($hari_kerja == $month->hari_kerja) 
            {
                $kerajinan = $karyawan->kerajinan;

            }else{

                $kerajinan = 0;
            }

            $model = new \App\Gaji;

            $model->karyawan_id = $karyawan->id;
            $model->bulan = $bulan;
            $model->tahun = $tahun;
            $model->hari_kerja = $hari_kerja;

            $kalkulasi = $pokok - $karyawan->cashbons()->whereMonth('tanggal', str_pad($bulan, 2, "0", STR_PAD_LEFT))->sum('nilai');

            if ($kalkulasi >= 0) 
            {
                $model->sisagaji = $kalkulasi;
                $model->sisacashbon = 0;
            }else{
                $model->sisagaji = 0;
                $model->sisacashbon = 0 - $kalkulasi;
            }

            $model->kerajinan = $kerajinan;
            $model->save();

            $bar->advance();
        }

        $bar->finish();

        $this->line('Selesai!');
    }
}
