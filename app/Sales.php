<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sales extends Model
{
	public $show = false;
	
    protected $fillable = ['text'];

    protected $hidden = ['id','created_at','updated_at'];

    protected $appends = ['posted'];

    public function getPostedAttribute()
    {
    	return $this->created_at->diffForHumans();
    }
}
