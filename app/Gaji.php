<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Events\GajiRetrieved;
use \Carbon\Carbon;

class Gaji extends Model
{
    //protected $fillable = ['bulan','tahun','hari_kerja','karyawan_id'];

    protected $hidden = ['id','created_at','updated_at','nilai_calc'];

    //protected $appends = ['nilai'];

    static $belongsTo = ['karyawan'];

    public $default_value;

	public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

		$this->default_value = [ 'tahun' => now()->format('Y') ];
	}

    public function karyawan()
    {
        return $this->belongsTo('App\Karyawan');
    }

    protected $dispatchesEvents = [
	    'created' => \App\Events\GajiRetrieved::class,
	    'retrieved' => \App\Events\GajiRetrieved::class,
	];

    // public function getNilaiAttribute()
    // {
    // 	if (! $this->nilai_calc <= 0) 
    // 	{
    // 		return $this->nilai_calc;
    // 	}

    // 	$bulan = \App\Bulan::where('tahun', $this->tahun)->where('bulan', $this->bulan)->first();

    // 	$pokok = $this->hari_kerja * $this->karyawan->harian;

    // 	if ($this->hari_kerja == $bulan->hari_kerja) 
    // 	{
    // 		$kerajinan = $this->karyawan->kerajinan;

    // 	}else{

    // 		$kerajinan = 0;
    // 	}

    // 	$calc = $pokok;
    // 	//$calc = $pokok + $kerajinan - $this->cashbon;

    // 	$model = $this;
    // 	$model->nilai_calc = $calc;
    // 	$model->save();

    // 	return $calc;
    // }
}
