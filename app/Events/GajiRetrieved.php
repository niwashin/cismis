<?php

namespace App\Events;

use App\Gaji;
use Illuminate\Queue\SerializesModels;

class GajiRetrieved
{
    use SerializesModels;

    public $gaji;

    /**
     * Create a new event instance.
     *
     * @param \App\User $user
     */
    public function __construct(Gaji $gaji)
    {
        $this->gaji = $gaji;

        $nilai = $gaji->nilai;
    }
}