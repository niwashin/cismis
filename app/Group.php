<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
	public $show = false;
	
    protected $fillable = [
        'name'
    ];
}
