<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait as SearchLocal;

class Karyawan extends Model
{
	use SearchLocal;

    protected $fillable = ['name','harian','bulanan','kerajinan'];

    protected $hidden = ['id','created_at','updated_at'];

    static $hasMany = ['gajis','gajis','cashbons'];

    protected $searchable = 
    [
        'columns' => [
            'name' => 10,
            'harian' => 5,
            'harian' => 5,
            'kerajinan' => 5,
        ],
    ];

    public function gajis()
    {
        return $this->hasMany('App\Gaji');
    }

    public function absens()
    {
        return $this->hasMany('App\Absen');
    }

    public function cashbons()
    {
        return $this->hasMany('App\Cashbon');
    }
}
