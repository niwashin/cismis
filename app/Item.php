<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    public $show = false;
    
    protected $fillable = ['name','price'];

    protected $casts = [
        'price' => 'float',
    ];

    static $validation_rule = [
    	'name' => 'required|unique:items,name|max:255',
        'price' => 'required|numeric',
    ];

    protected $searchableColumns = [
        'name' => 20,
    ];
}
